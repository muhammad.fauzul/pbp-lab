from django import forms
from django.db import models
from django.forms import fields, widgets
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
    widgets = {
        'to' : fields.CharField(),
        'From' : fields.CharField(),
        'title' : fields.CharField(),
        'message' : widgets.TextInput(), 
    }