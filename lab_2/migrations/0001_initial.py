# Generated by Django 3.2.7 on 2021-09-23 13:59

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('to', models.CharField(max_length=30)),
                ('frm', models.CharField(max_length=30)),
                ('title', models.CharField(max_length=40)),
                ('message', models.TextField()),
            ],
        ),
    ]
