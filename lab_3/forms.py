from lab_1.models import Friend
from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm (forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"

    name = forms.CharField(label='Name', max_length=30)
    npm = forms.CharField(label='NPM', max_length=10)
    # DOB = forms.DateField(label='Date of Birth')
    widgets = {'DOB': DateInput(),}
    