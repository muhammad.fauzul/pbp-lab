// ignore_for_file: prefer_const_constructors, deprecated_member_use

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Widget> widgets = [];

  _MyAppState() {
    for (int i = 1; i <= 10; i++) {
      widgets.add(Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.fromLTRB(20, 3, 20, 2),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5), color: Colors.lightBlue),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Topic ke-" + i.toString(),
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Description Topic ke-" + i.toString(),
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
            RaisedButton(
              onPressed: () {},
              child: Text("open"),
            )
          ],
        ),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.fallback(),
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Forum Discussion"),
        ),
        body: ListView(
          children: [
            Center(
              child: Text(
                "Topic List",
                style: TextStyle(fontSize: 40),
              ),
            ),
            Column(
              children: widgets,
            ),
          ],
        ),
      ),
    );
  }
}
